
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?= $data['title']?></h3>
                <a href="admin/addwarehouse" class="btn btn-primary">Thêm mới</a>
                <h3 class="text-success"><?=$data["mess"]?></h3>
                <h3 class="text-danger"><?=$data["messfail"]?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="x_content">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table class="table table-striped jambo_table bulk_action" id="datatable-keytable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr class="headings">
                            <th class="column-title">Id</th>
                            <th class="column-title">Địa chỉ</th>
                            <th class="column-title">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data["data"] as $key => $values){?>
                            <tr>
                            <td><?=$values["Id"]?></td>
                            <td><?=$values["Address"] ?></td>
                            <td>
                                <a style="height: 35px;" class="btn btn-success" href="<?=base?>admin/editwarehouse&id=<?=$values['Id']?>">Sửa</a>
                                <a style="height: 35px" class="btn btn-danger submit" href="javascrip:void(0)" onclick="del(<?=$values['Id']?>,'','<?=base.'admin/deletewarehouse/'?>','')"  >Xóa</a> 
                            </td>
                            </tr>
                            <?php }?>
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>