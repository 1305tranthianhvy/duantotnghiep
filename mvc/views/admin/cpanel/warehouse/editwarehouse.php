<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?= $data['title']?></h3>
            <a href="admin/showwarehouse" class="btn btn-primary">Trở Về</a>
            <h3 class="text-success"><?= $data["mess"]?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="x_content">
        <form class="" action="" method="post" novalidate>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="">Địa chỉ kho</label>
                        <input id="address" type="text" class="form-control" value="<?=$data['data'][0]['Address']?>" name="address" placeholder="Nhập địa chỉ kho">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit">Cập nhật</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>