<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?= $data['title']?></h3>
            <a href="admin/showsupplier" class="btn btn-primary">Trở Về</a>
            <h3 class="text-success"><?= $data["mess"]?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="x_content">
        <form class="" action="" method="post" novalidate>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="production_Company">Tên công ty</label>
                        <input id="production_Company" type="text" class="form-control" value="<?=$data['data'][0]['production_Company']?>" name="production_Company" placeholder="Nhập địa chỉ kho" required>
                    </div>
                    <div class="form-group">
                        <label for="Id_ware_house">Chọn nhà cung cấp</label>
                        <select id="Id_ware_house"  class="form-control" name="Id_ware_house">
                            <?php foreach($data['datawarehouse'] as $row) {?>
                            <option <?php ($data['data'][0]['Id_ware_house'] == $row['Id'])?"selected":""?>   value="<?=$row['Id']?>"><?=$row['Address']?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit">Cập nhật</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>