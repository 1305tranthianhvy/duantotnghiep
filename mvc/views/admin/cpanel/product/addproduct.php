<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?= $data['title']?></h3>
            <a class="btn btn-primary" href="<?=base?>admin/showproduct">Trở Về</a>
                <h4 class="text-success"><?php if(isset($data["addsuccess"])){echo $data["addsuccess"];} ?></h4>
                <h6 class="text-danger"><?php if($data["notifi"] != null){NotificationRight("Vui Lòng điền đẩy đủ thông tin","top-end");}?> </h6>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="x_content">
        <form class="" action="" method="post" novalidate enctype="multipart/form-data">
            <div class="row" >
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Tên Sản Phẩm</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["name"])){echo $data["notifi"]["name"];}?> </small>
                        <input id="name" type="text" class="form-control" name="product[name]">                   
                    </div>
                  
                    <div class="form-group">
                        <label for="">Chọn Danh Mục Sản Phẩm</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["category"])){echo $data["notifi"]["category"];}?> </small>
                        <select class="form-control" name="product[id_category]">
                            <option value="true">-----Chọn Danh Mục Sản Phẩm-----</option>
                            <?php foreach($data["data"] as $key=>$values){?>
                            <option value="<?=$values["id"]?>"><?=$values["name"]?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Nhà Sản Xuất</label>
                        <input id="name" type="text" class="form-control" name="product[company]">
                    </div>
                    
                </div>
                <!-- trong row chứa col , trong col chứa dc row -->
                <!-- col từ 1 -> 12 -->
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Giá Sản Phẩm</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["price"])){echo $data["notifi"]["price"];}?> </small>
                        <input id="" type="number" class="form-control" name="product[price]">
                    </div>
                    <div class="form-group">
                        <label for="">Giảm Giá</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["sale"])){echo $data["notifi"]["sale"];}?> </small>
                        <input id="name" type="number" class="form-control" name="product[sale]" placeholder="%">
                    </div>
                    <div class="form-group">
                        <label for="">Số Lượng</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["quantity"])){echo $data["notifi"]["quantity"];}?> </small>
                        <input id="name" type="number" class="form-control" name="product[quantity]">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="">Hình Ảnh</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["img"])){echo $data["notifi"]["img"];}?> </small>
                        <input id="name" type="file" accept=".jpg, .png" class="form-control" name="img">
                    </div>
                    <div class="form-group">
                        <label for="">Tồn kho</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["quantity_inv"])){echo $data["notifi"]["quantity_inv"];}?> </small>
                        <input id="name" type="number" value="0" class="form-control" name="product[quantity_inv]" required>
                    </div>
                    <div class="form-group">
                        <label for="">Chọn kho</label><br>
                        <small class="text-danger"><?php if(isset($data["notifi"]["ware_house"])){echo $data["notifi"]["ware_house"];}?> </small>
                        <select class="form-control" name="product[id_ware_house]">
                            <option value="true">-----Chọn Danh Kho-----</option>
                            <?php foreach($data["ware_house"] as $key=>$values){?>
                                <option value="<?=$values["Id"]?>"><?=$values["Address"]?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        <label for="">Mô Tả</label>
                        <textarea style="height: 100px;" id="name" type="text" class="form-control" name="product[decs]"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" name="submit">Thêm Sản Phẩm</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
