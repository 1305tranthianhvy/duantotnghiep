<?php

    class sigin extends controller{
        var $usermodel;
        var $commonmodel;
        var $accountmodel;
        function __construct()
        {
            $this->usermodel = $this->ModelClient("usermodel");
            $this->commonmodel = $this->ModelCommon("commonmodel");
            $this->accountmodel = $this->ModelAdmin("accountmodel");
        }

        function error404(){
            $data = [];
            $this->ViewAdmin("error404",$data);
        }
        
        function sigin(){
            if(!isset($_SESSION["info"])){
                $mess = "";
                if(isset($_POST["sigin"])){
                    $post = $_POST["data"];
                    if($post["pass"] == $post["pass_confirm"]){
                        $checkuser = $this->commonmodel->checkemail($post["email"]);
                        if($checkuser < 1){
                            $user = $this->commonmodel->sigin($post["email"],md5($post["pass"]),$post["name"],$post["address"],$post["phonenumber"]);
                            if($user){
                                $name = $this->accountmodel->GetNameUser($post["email"],md5($post["pass"]));
                                $_SESSION["info"]["id"]= $name[0]["id"];
                                $_SESSION["info"]["name"] = $name[0]["name"];
                                $_SESSION["info"]["phone"] = $name[0]["phone_number"];
                                $_SESSION["info"]["address"] = $name[0]["address_user"];
                                $_SESSION["info"]["email"] = $name[0]["email_account"];
                                NotifiSiginSuccess();
                                header('Refresh: 1; URL='.base.'');
                            }
                        }else{
                            $mess = "<p style='color: red;'>Email này đã có người khác sử dụng</p>";
                        }
                    }else{
                        $mess = "<p style='color: red;'>Xác nhận mật khẩu không khớp</p>";
                    }
                }
                $data = ["mess"=>$mess];
                $this->ViewClinet("sigin",$data);
            }else header("location:".base."home/index");
        }

    }
?>