<?php
    function UpLoadFiles($url,$file = [], $nameimg=null){
        //tạo đường dẫn khi upload lên sever
        // echo "<pre>";
        // print_r($file);die;
        
            $target_file = $url.basename($nameimg);
        //kiểm tra dung lượng của file
        $error =[];
       
            if($file["img"]["size"] > 5242880){
                $error = ["size"=>"file ".$nameimg." vượt quá 5MB"];
            }

        //Kiểm tra loại file
        
            $file_type = pathinfo($nameimg, PATHINFO_EXTENSION);
            $array_file_type = ["jpg","png","jpeg","gif"];
            if(!in_array(strtolower($file_type),$array_file_type)){
                $error = ["file ".$nameimg." không đúng định dạng"];
            }
        
        //Kiểm tra đã tồn tại trên sever hay chưa
        
            if(file_exists($target_file)){
                $error = ["exits"=>"file ".$nameimg." đã tồn tại trên hệ thống"];
            }
        //upload lên sever
        if(empty($error)){
                if(move_uploaded_file($file["img"]["tmp_name"],SITE_ROOT.$target_file)){
                    $success = true;
                }
        }else{
            return $error;
        }
        return $success;
    }
?>