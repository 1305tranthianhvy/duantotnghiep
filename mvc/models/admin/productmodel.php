<?php

    class productmodel extends database{
        //Thêm sản phẩm
        function AddProduct($name,$price,$img,$quantity,$decsription,$company,$id_category,$name_category,$sale){
            $sql = "insert into product (name, price, img_product, quantity, descrip, production_company, create_at, update_at, 
            category_id, name_category, pay, sale_product, status_delete) values 
            ('$name','$price','$img',$quantity,'$decsription','$company',current_time(), current_time(), $id_category,'$name_category',
            0,$sale,0)";
            $query = $this->conn->prepare($sql);
            $result = $query->execute();
            $last_id = $this->conn->lastInsertId();
            return $last_id;
        }
        //Xóa sản phẩm
        function DeleteProduct($id){
            $sql = "UPDATE product SET status_delete = 1 WHERE id = '$id'";
            $query = $this->conn->prepare($sql);
            $query->execute();
            return $query;
        }
        //xóa những sản phẩm thuộc danh mục bị xóa
        function UpdateProduct($id){
            $sql =  "UPDATE  product SET status_delete = 1 where category_id = $id";
            $query = $this->conn->prepare($sql);
            $query->execute();
            return $query;
        }
        // lấy sản phẩm theo id với số lượng là litmit và bắt đầu từ offset
        function GetProductPage($id,$limit,$offset){
            $sql = "SELECT * FROM product where category_id = $id and status_delete = 0 ORDER BY 'id' ASC LIMIT $limit OFFSET $offset";
            $query = $this->conn->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return json_decode(json_encode($result),true);
        }
        //lấy số lượng sản phẩm theo danh mục
        function GetNumber($id){
            $sql = "SELECT * FROM product where category_id = $id and status_delete = 0";
            $query = $this->conn->prepare($sql);
            $query->execute();
            $result =  $query->rowCount();
            return $result;
        }
        // cập nhật lại sản phẩm sau khi sửa
        function UpdateProductById($id,$name,$price,$img,$quantity,$decsrip,$company,$id_category,$name_category, $sale){
            $sql = "UPDATE product SET price = $price, img_product = '$img', quantity = $quantity, production_company = '$company', descrip = '$decsrip', update_at = current_time(), category_id = $id_category, name_category = '$name_category', sale_product='$sale', name = '$name' WHERE id = $id";
            $query = $this->conn->prepare($sql);
            $result = $query->execute();
            return $result;
        }

        function showproductwithinventory($table){
            $sql = "SELECT p.*, inv.Quantity as quantityINV, inv.Id_ware_house, w.Address FROM $table as p 
                LEFT JOIN inventory as inv ON p.id = inv.Id_product
                LEFT JOIN ware_house as w ON w.Id = inv.Id_ware_house
                WHERE p.status_delete = 0";
            $query = $this->conn->prepare($sql);
            $query->execute();
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return json_encode($result);
        }
        
        function CheckProduct($id){
            $sql = "SELECT * FROM inventory where Id_product = $id";
            $query = $this->conn->prepare($sql);
            $query->execute();
            $result =  $query->rowCount();
            return $result;
        }
    }
?>